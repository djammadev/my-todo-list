#!/usr/bin/env bash

export DATABASE_USERNAME=root
export DATABASE_PASSWORD=complicate-password
export DATABASE_DRIVER=org.h2.Driver
export DATABASE_NAME=my-todo-list
export DATABASE_URL="jdbc:h2:file:./my-todo-list;MODE=MYSQL;DB_CLOSE_DELAY=-1"

export EBEAN_MIGRATION_GENERATE=true
export EBEAN_MIGRATION_RUN=true
export EBEAN_MIGRATION_PATH=dbmigration/mysql
export EBEAN_MIGRATION_NAME=migration

export MAIL_MOCK=true
