package com.loreal.application;

import com.loreal.security.AuthFilter;
import com.loreal.security.TodolistAuthenticator;
import com.loreal.security.TodolistBearerAuthenticator;
import com.shinitech.djammadev.application.STApplicationBase;
import com.shinitech.djammadev.resources.CRUDResource;
import org.glassfish.jersey.server.filter.RolesAllowedDynamicFeature;

import javax.annotation.security.DeclareRoles;
import javax.ws.rs.ApplicationPath;

import static com.shinitech.djammadev.filter.STAuthenticatorFilter.AUTHENTICATOR_PROPERTY_BASIC;
import static com.shinitech.djammadev.filter.STAuthenticatorFilter.AUTHENTICATOR_PROPERTY_BEARER;

/**
 * @author Djamma Dev by sissoko
 * @date 2021-05-27
 */
@ApplicationPath("/")
@DeclareRoles({"ADMIN", "USER"})
public class MyTodoListApplication extends STApplicationBase {
    public MyTodoListApplication() {
        packages("com.loreal.resources")
                .register(CRUDResource.class)
                .register(RolesAllowedDynamicFeature.class)
                .register(AuthFilter.class)
                .property(AUTHENTICATOR_PROPERTY_BASIC, new TodolistAuthenticator())
                .property(AUTHENTICATOR_PROPERTY_BEARER, new TodolistBearerAuthenticator());
    }
}
