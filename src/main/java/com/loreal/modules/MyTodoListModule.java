package com.loreal.modules;

import com.google.inject.AbstractModule;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

/**
 * @author Djamma Dev by ${user}
 * @date ${date}
 */
public class MyTodoListModule extends AbstractModule {
    @Override
    protected void configure() {
        final Config config = ConfigFactory.load();
        bind(Config.class).toInstance(config);
    }
}
