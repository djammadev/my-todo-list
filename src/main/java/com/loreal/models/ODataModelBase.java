package com.loreal.models;

import com.avaje.ebean.Model;
import com.avaje.ebean.annotation.WhenCreated;
import com.avaje.ebean.annotation.WhenModified;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.shinitech.djammadev.models.STModel;
import com.shinitech.djammadev.odata.dataprovider.ODataProvider;
import com.shinitech.djammadev.odata.models.ODataEntity;
import com.shinitech.djammadev.odata.models.ODataProperty;
import com.shinitech.djammadev.services.provider.Json;
import com.shinitech.djammadev.services.provider.STObjectMapper;
import org.apache.olingo.commons.api.data.Linked;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.net.URI;
import java.util.Date;

@MappedSuperclass
public abstract class ODataModelBase extends Model implements STModel, ODataEntity {

    private static final ObjectMapper mapper = new STObjectMapper();
    @Id
    @ODataProperty(ref = true)
    public Long id;
    @ODataProperty(type = "Edm.DateTimeOffset")
    @WhenCreated
    public Date createDate;
    @ODataProperty(type = "Edm.DateTimeOffset")
    @WhenModified
    public Date modifyDate;
    @ODataProperty
    public Boolean active;
    @ODataProperty
    @Column(columnDefinition = "VARCHAR(255) DEFAULT 'UNKNOW'")
    public String createdBy;
    @ODataProperty
    @Column(columnDefinition = "VARCHAR(255) DEFAULT 'UNKNOW'")
    public String modifiedBy;
    @ODataProperty
    public Long ownerId;

    public ODataModelBase() {
        super();
        this.createDate = new Date();
        this.modifyDate = new Date();
        this.active = true;
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public boolean isActive() {
        return active == Boolean.TRUE;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    @Override
    public Long getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(Long ownerId) {
        this.ownerId = ownerId;
    }

    public JsonNode getOptions() {
        return Json.newObject();
    }

    @Override
    public String toString() {
        return tryToString();
    }

    private String tryToString() {
        try {
            return mapper.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            throw new IllegalArgumentException(e.getMessage(), e);
        }
    }

    public <T extends ODataEntity> Linked getEntity(Class<T> tClass, T $this) {
        return getEntity(tClass, $this, true);
    }

    public <T extends ODataEntity> Linked getEntity(Class<?> tClass, T $this, boolean withRelated) {
        return ODataProvider.getEntity(tClass, $this, withRelated);
    }

    @Override
    public URI entityId() {
        return URI.create(String.format(template(), getId()));
    }

    public abstract String template();
}
