package com.loreal.models;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.loreal.models.finder.UserFinder;
import com.shinitech.djammadev.annotations.CRUD;
import com.shinitech.djammadev.models.STUser;
import com.shinitech.djammadev.odata.models.ODataEntity;
import com.shinitech.djammadev.odata.models.ODataModel;
import com.shinitech.djammadev.odata.models.ODataNavigationProperty;
import com.shinitech.djammadev.odata.models.ODataProperty;
import com.shinitech.djammadev.services.provider.STSerializer;
import org.apache.olingo.commons.api.data.Linked;

import javax.persistence.*;
import java.util.List;

/**
 * @author Djamma Dev by sissoko
 * @date 2021-05-27
 */
@Entity
@Table(name = "t_users")
@JsonSerialize(using = User.Serializer.class)
@CRUD(value = "Users", path = "user")
@ODataModel(value = "Users", container = "com.loreal.models.Container")
public class User extends ODataModelBase implements ODataEntity, STUser {
    public static final UserFinder find = new UserFinder();
    @ODataProperty
    @Column(unique = true)
    public String username;
    @ODataProperty
    public String password;
    @ODataProperty
    @Column(nullable = false)
    public String firstName;
    @ODataProperty
    @Column(nullable = false)
    public String lastName;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "user")
    @ODataNavigationProperty(type = UserRole.class, collection = true)
    public List<UserRole> roles;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "user")
    @ODataNavigationProperty(type = TodoList.class, collection = true)
    public List<TodoList> todoLists;

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Override
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public List<UserRole> getRoles() {
        return roles;
    }

    public void setRoles(List<UserRole> roles) {
        this.roles = roles;
    }

    public List<TodoList> getTodoLists() {
        return todoLists;
    }

    public void setTodoLists(List<TodoList> todoLists) {
        this.todoLists = todoLists;
    }

    @Override
    public String[] roles() {
        if (getRoles() == null)
            return new String[]{};
        String[] rolesArray = new String[getRoles().size()];
        for (int i = 0; i < getRoles().size(); i++) {
            rolesArray[i] = getRoles().get(i).getRole();
        }
        return rolesArray;
    }

    @Override
    public String template() {
        return "Users(%d)";
    }

    @Override
    public Linked toEntity() {
        return getEntity(User.class, this);
    }

    static class Serializer extends STSerializer<User> {
    }
}
