package com.loreal.models.finder;

import com.loreal.models.Task;
import com.shinitech.djammadev.models.finder.FinderBase;

/**
 * Djamma Dev
 * Created by sissoko on 27/05/2021 07:36.
 */
public class TaskFinder extends FinderBase<Task> {

    public TaskFinder() {
        super(Task.class);
    }
}
