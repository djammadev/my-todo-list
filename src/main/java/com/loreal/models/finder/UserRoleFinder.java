package com.loreal.models.finder;

import com.loreal.models.UserRole;
import com.shinitech.djammadev.models.finder.FinderBase;

/**
 * Djamma Dev
 * Created by sissoko on 24/05/2021 21:59.
 */
public class UserRoleFinder extends FinderBase<UserRole> {

    public UserRoleFinder() {
        super(UserRole.class);
    }
}
