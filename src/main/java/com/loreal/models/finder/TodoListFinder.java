package com.loreal.models.finder;

import com.loreal.models.TodoList;
import com.shinitech.djammadev.models.finder.FinderBase;

/**
 * Djamma Dev
 * Created by sissoko on 27/05/2021 07:26.
 */
public class TodoListFinder extends FinderBase<TodoList> {

    public TodoListFinder() {
        super(TodoList.class);
    }
}
