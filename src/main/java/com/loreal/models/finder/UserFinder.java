package com.loreal.models.finder;

import com.shinitech.djammadev.models.finder.FinderBase;
import com.loreal.models.User;

/**
 * @author Djamma Dev by ${user}
 * @date ${date}
 */
public class UserFinder extends FinderBase<User> {

    public UserFinder() {
        super(User.class);
    }
}
