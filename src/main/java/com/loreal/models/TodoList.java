package com.loreal.models;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.shinitech.djammadev.annotations.CRUD;
import com.shinitech.djammadev.odata.models.*;
import com.shinitech.djammadev.services.provider.JsonIncludeProperties;
import com.shinitech.djammadev.services.provider.STSerializer;

import com.loreal.models.finder.TodoListFinder;
import org.apache.olingo.commons.api.data.Linked;

import javax.persistence.*;
import java.util.List;

/**
 * Djamma Dev
 * Created by sissoko on 27/05/2021 07:25.
 */
@Entity
@Table(name = "t_todo_lists")
@CRUD(value = "TodoLists", path = "todolist")
@JsonSerialize(using = TodoList.Serializer.class)
@ODataModel(value = "Todolists", container = "com.loreal.models.Container")
public class TodoList extends ODataModelBase implements ODataEntity {

    public static final TodoListFinder find = new TodoListFinder();

    @ODataProperty
    public String title;
    @ManyToOne
    @JoinColumn(name = "user_id")
    @ODataNavigationProperty(type = User.class, keyName = "userId")
    public User user;
    @Transient
    @ODataTransient(type = User.class, binding = "user")
    public Long userId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "todoList")
    @ODataNavigationProperty(type = Task.class, collection = true)
    public List<Task> tasks;

    public TodoList() {
        super();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @JsonIncludeProperties("id")
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }

    @Override
    public String template() {
        return "Todolists(%d)";
    }

    @Override
    public Linked toEntity() {
        return getEntity(TodoList.class, this);
    }

    static class Serializer extends STSerializer<TodoList> {
    }
}
