package com.loreal.models;

import com.avaje.ebean.annotation.Cache;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.loreal.models.finder.UserRoleFinder;
import com.shinitech.djammadev.annotations.CRUD;
import com.shinitech.djammadev.odata.models.*;
import com.shinitech.djammadev.services.provider.JsonIncludeProperties;
import com.shinitech.djammadev.services.provider.STSerializer;
import org.apache.olingo.commons.api.data.Linked;

import javax.persistence.*;

/**
 * @author Djamma Dev by sissoko
 * @date 27/09/2020
 */
@Cache
@Entity
@Table(name = "as_user_roles", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"USERNAME", "ROLE"})
})
@JsonSerialize(using = UserRole.Serializer.class)
@CRUD(value = "User Roles", path = "user-role")
@ODataModel(value = "UserRoles", container = "com.loreal.models.Container")
public class UserRole extends ODataModelBase implements ODataEntity {
    public static final UserRoleFinder find = new UserRoleFinder();
    @ODataProperty
    @Column(name = "username", nullable = false)
    public String username;
    @ODataProperty
    @Column(name = "role", nullable = false)
    public String role;
    @ManyToOne
    @JoinColumn(name = "user_id")
    @ODataNavigationProperty(type = User.class, keyName = "userId")
    public User user;
    @Transient
    @ODataTransient(type = User.class, binding = "user")
    public Long userId;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @JsonIncludeProperties("id")
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Override
    public String template() {
        return "UserRoles(%d)";
    }

    @Override
    public Linked toEntity() {
        return getEntity(UserRole.class, this);
    }

    static class Serializer extends STSerializer<UserRole> {
    }
}
