package com.loreal.models;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.loreal.models.finder.TaskFinder;
import com.shinitech.djammadev.annotations.CRUD;
import com.shinitech.djammadev.odata.models.*;
import com.shinitech.djammadev.services.provider.JsonIncludeProperties;
import com.shinitech.djammadev.services.provider.STSerializer;
import org.apache.olingo.commons.api.data.Linked;

import javax.persistence.*;

/**
 * Djamma Dev
 * Created by sissoko on 27/05/2021 07:36.
 */
@Entity
@Table(name = "t_tasks")
@CRUD(value = "Tasks", path = "task")
@JsonSerialize(using = Task.Serializer.class)
@ODataModel(value = "Tasks", container = "com.loreal.models.Container")
public class Task extends ODataModelBase implements ODataEntity {

    public static final TaskFinder find = new TaskFinder();

    @ODataProperty
    public String title;
    @ODataProperty
    public String description;
    @ManyToOne
    @JoinColumn(name = "todolist_id")
    @ODataNavigationProperty(type = TodoList.class, keyName = "todolistId")
    public TodoList todoList;
    @Transient
    @ODataTransient(type = TodoList.class, binding = "todoList")
    public Long todolistId;

    public Task() {
        super();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @JsonIncludeProperties("id")
    public TodoList getTodoList() {
        return todoList;
    }

    public void setTodoList(TodoList todoList) {
        this.todoList = todoList;
    }

    public Long getTodolistId() {
        return todolistId;
    }

    public void setTodolistId(Long todolistId) {
        this.todolistId = todolistId;
    }

    @Override
    public String template() {
        return "Tasks(%d)";
    }

    @Override
    public Linked toEntity() {
        return getEntity(Task.class, this);
    }

    static class Serializer extends STSerializer<Task> {
    }
}
