package com.loreal.resources;

import com.loreal.models.User;
import com.loreal.models.finder.UserFinder;

import com.shinitech.djammadev.interceptor.SecuredSession;
import com.shinitech.djammadev.resources.AbstractEntityResource;
import com.shinitech.djammadev.services.annotations.DDApi;

import io.swagger.annotations.Api;

import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import static javax.ws.rs.core.MediaType.*;

/**
 * @author Djamma Dev by ${user}
 * @date ${date}
 */
@Path("/user")
@Api("User")
@DDApi(User.class)
@Produces({APPLICATION_JSON, TEXT_PLAIN})
@Consumes({APPLICATION_JSON})
@SecuredSession("ADMIN")
public class UserResource extends AbstractEntityResource<User> {

    public UserResource() {
        super(User.class);
    }

    @Override
    protected UserFinder find() {
        return User.find;
    }
}
