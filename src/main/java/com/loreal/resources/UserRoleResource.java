package com.loreal.resources;

import com.avaje.ebean.Finder;
import com.loreal.models.UserRole;
import com.loreal.models.finder.UserRoleFinder;
import com.shinitech.djammadev.interceptor.SecuredSession;
import com.shinitech.djammadev.resources.AbstractEntityResource;
import com.shinitech.djammadev.resources.AbstractResource;
import com.shinitech.djammadev.services.annotations.DDApi;
import io.swagger.annotations.Api;

import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import static javax.ws.rs.core.MediaType.*;

/**
 * Djamma Dev
 * Created by sissoko on 27/05/2021 07:53.
 */
@Path("/user-role")
@Api("UserRole")
@DDApi(UserRole.class)
@Produces({APPLICATION_JSON, TEXT_PLAIN})
@Consumes({APPLICATION_JSON})
@SecuredSession("ADMIN")
public class UserRoleResource extends AbstractEntityResource<UserRole> {

    public UserRoleResource() {
        super(UserRole.class);
    }

    @Override
    public UserRole doInit() {
        return new UserRole();
    }

    @Override
    protected UserRoleFinder find() {
        return UserRole.find;
    }

    @Override
    protected String getBaseKey() {
        return "user-role";
    }
}
