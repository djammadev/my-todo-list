package com.loreal.resources;

import com.loreal.models.TodoList;
import com.loreal.models.User;
import com.loreal.models.finder.TodoListFinder;
import com.shinitech.djammadev.interceptor.SecuredSession;
import com.shinitech.djammadev.resources.AbstractEntityResource;
import com.shinitech.djammadev.services.annotations.DDApi;
import io.swagger.annotations.Api;

import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import java.util.List;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.MediaType.TEXT_PLAIN;

/**
 * Djamma Dev
 * Created by sissoko on 27/05/2021 07:52.
 */
@Path("/todolist")
@Api("TodoList")
@DDApi(TodoList.class)
@Produces({APPLICATION_JSON, TEXT_PLAIN})
@Consumes({APPLICATION_JSON})
@SecuredSession
public class TodoListResource extends AbstractEntityResource<TodoList> {

    public TodoListResource() {
        super(TodoList.class);
    }

    @Override
    public void beforeInsert(List<TodoList> beanList) {
        Long ownerId = getConnected().getId();
        beanList.forEach(todoList -> todoList.setOwnerId(ownerId));
        super.beforeInsert(beanList);
    }

    @Override
    public TodoList doInit() {
        return new TodoList();
    }

    @Override
    protected TodoListFinder find() {
        return TodoList.find;
    }

    @Override
    protected String getBaseKey() {
        return "todolist";
    }
}
