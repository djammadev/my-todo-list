package com.loreal.resources;

import com.loreal.models.Task;
import com.loreal.models.TodoList;
import com.loreal.models.finder.TaskFinder;
import com.shinitech.djammadev.interceptor.SecuredSession;
import com.shinitech.djammadev.resources.AbstractEntityResource;
import com.shinitech.djammadev.services.annotations.DDApi;
import io.swagger.annotations.Api;

import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import java.util.List;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.MediaType.TEXT_PLAIN;

/**
 * Djamma Dev
 * Created by sissoko on 27/05/2021 07:53.
 */
@Path("/task")
@Api("Task")
@DDApi(Task.class)
@Produces({APPLICATION_JSON, TEXT_PLAIN})
@Consumes({APPLICATION_JSON})
@SecuredSession
public class TaskResource extends AbstractEntityResource<Task> {

    public TaskResource() {
        super(Task.class);
    }


    @Override
    public void beforeInsert(List<Task> beanList) {
        Long ownerId = getConnected().getId();
        beanList.forEach(todoList -> todoList.setOwnerId(ownerId));
        super.beforeInsert(beanList);
    }

    @Override
    public Task doInit() {
        return new Task();
    }

    @Override
    protected TaskFinder find() {
        return Task.find;
    }

    @Override
    protected String getBaseKey() {
        return "task";
    }
}
