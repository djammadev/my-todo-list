package com.loreal.resources;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.google.inject.Injector;
import com.loreal.models.User;
import com.loreal.models.finder.UserFinder;
import com.shinitech.djammadev.forms.AccessToken;
import com.shinitech.djammadev.forms.Credentials;
import com.shinitech.djammadev.forms.STokenizerData;
import com.shinitech.djammadev.models.templates.Template;
import com.shinitech.djammadev.resources.AbstractAuthenticationResource;
import com.shinitech.djammadev.services.security.Hash;
import com.shinitech.djammadev.services.tools.Pair;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.validator.constraints.NotBlank;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import java.io.UnsupportedEncodingException;
import java.security.Principal;
import java.util.Base64;
import java.util.Collections;
import java.util.Date;

import static javax.ws.rs.core.HttpHeaders.AUTHORIZATION;
import static javax.ws.rs.core.MediaType.*;

@Path("/auth")
@Api("Authentication Resource")
@Produces({APPLICATION_JSON, TEXT_PLAIN})
@Consumes({APPLICATION_JSON, APPLICATION_FORM_URLENCODED})
public class AuthenticationResource extends AbstractAuthenticationResource<User> {
    private static final Logger LOGGER = LogManager.getLogger(AuthenticationResource.class);
    @Context
    protected HttpServletRequest request;
    @Inject
    @Named("bcrypt")
    protected Hash hash;

    private final Injector injector;

    @Inject
    public AuthenticationResource(Injector injector) {
        this.injector = injector;
    }

    protected Principal getUserPrincipal() {
        return requestContext.getSecurityContext().getUserPrincipal();
    }

    protected String getUsername() {
        Principal principal = getUserPrincipal();
        return principal != null ? principal.getName() : null;
    }

    @Override
    @RolesAllowed({"ADMIN", "USER"})
    public Response fetchUser() {
        User user = getConnected();
        return Response.ok(user).build();
    }

    public User getConnected() {
        return User.find.by("username", getUsername());
    }

    @POST
    @PermitAll
    @ApiOperation("Login into the application")
    public Response logIn(@Valid Credentials credentials) {
        User user = User.find.by("username", credentials.getUsername());
        if (user == null || !hash.checkPassword(credentials.getPassword(), user.getPassword())) {
            throw new BadRequestException("Username or password incorrect");
        }
        String token = Base64.getEncoder().encodeToString(String.format("%s:%s", credentials.getUsername(), credentials.getPassword()).getBytes());
        HttpSession session = request.getSession(true);
        session.setAttribute(X_AUTH_TOKEN, token); // Store Session
        user.save(); // Update last connexion date.
        return Response.ok(user)
                .header(AUTHORIZATION, String.format("Basic %s", token))
                .header(X_AUTH_TOKEN, token)
                .build();
    }

    protected User getUser(String token, String code) {
        String secret = application.getString(API_KEY);
        try {
            DecodedJWT jwt = JWT.decode(token);
            Date expiresAt = jwt.getExpiresAt();
            if (new Date().after(expiresAt)) {
                throw new BadRequestException("Token expired");
            }
            String sign = JWT.create()
                    .withIssuer(jwt.getIssuer())
                    .withSubject(jwt.getSubject())
                    .withIssuedAt(jwt.getIssuedAt())
                    .withClaim("code", code)
                    .withExpiresAt(expiresAt)
                    .sign(Algorithm.HMAC256(secret));
            if (!sign.endsWith(jwt.getSignature()))
                throw new BadRequestException("Invalid code");
            return find().by("username", jwt.getSubject());
        } catch (JWTDecodeException | UnsupportedEncodingException e) {
            LOGGER.error(e.getMessage(), e);
            throw new BadRequestException(e.getMessage());
        }
    }

    @Override
    public Response register(User user) {
        throw new NotAuthorizedException("Register is not allowed.");
    }

    @Override
    public Response registerAccessToken(STokenizerData<User> data) {
        throw new NotAuthorizedException("Register is not allowed.");
    }

    @Override
    protected UserFinder find() {
        return User.find;
    }
}
