package com.loreal.edmprovider;

import com.shinitech.djammadev.odata.edmprovider.ODataEdmProvider;

import java.util.Set;

/**
 * @author Djamma Dev by sissoko
 * @date 04/10/2020
 */
public class TodoListEdmProvider extends ODataEdmProvider {

    public static final String NAMESPACE = "com.loreal.models";
    public static final String CONTAINER = "Container";

    public TodoListEdmProvider(Set<Class<?>> classes) {
        super(classes);
        this.containerName = CONTAINER;
        this.namespace = NAMESPACE;
    }
}
