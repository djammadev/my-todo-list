package com.loreal.server;

import com.loreal.edmprovider.TodoListEdmProvider;
import com.shinitech.djammadev.odata.edmprovider.ODataEdmProvider;
import com.shinitech.djammadev.odata.server.ODataServletBase;

/**
 * @author Djamma Dev by sissoko
 * @date 03/10/2020
 */
public class MyTodolistServlet extends ODataServletBase {
    public MyTodolistServlet() {
        registerModel("com.loreal.models");
        registerProcessor("com.shinitech.djammadev.odata.processor");
    }

    @Override
    public ODataEdmProvider getEdmProvider() {
        return new TodoListEdmProvider(getModels());
    }
}
