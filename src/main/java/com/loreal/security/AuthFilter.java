package com.loreal.security;

import com.shinitech.djammadev.filter.AuthenticationFilter;
import org.apache.commons.lang3.StringUtils;

import javax.annotation.Priority;
import javax.annotation.security.PermitAll;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.ext.Provider;
import java.lang.reflect.Method;

import static com.shinitech.djammadev.resources.AbstractAuthenticationResource.X_AUTH_TOKEN;

@Provider
@Priority(Priorities.AUTHENTICATION)
public class AuthFilter extends AuthenticationFilter {

    @Context
    private ResourceInfo resourceInfo;
    @Context
    private HttpServletRequest request;

    @Override
    public void filter(ContainerRequestContext requestContext) {
        Method method = resourceInfo.getResourceMethod();
        Class<?> resourceClass = resourceInfo.getResourceClass();
        if (method.isAnnotationPresent(PermitAll.class)) {
            return;
        }
        if (resourceClass.isAnnotationPresent(PermitAll.class)) {
            return;
        }
        HttpSession session = this.request.getSession(true);
        String token = (String) session.getAttribute(X_AUTH_TOKEN);
        System.out.println("---- token ---- " + token);
        System.out.println("---- authorization ---- " + requestContext.getHeaderString(HttpHeaders.AUTHORIZATION));
        if (StringUtils.isNotBlank(token)) {
            String scheme = "Basic";
            String[] parts = token.split(" ");
            if (parts.length > 1) {
                scheme = parts[0];
                token = parts[1];
            }
            requestContext.getHeaders().putSingle(HttpHeaders.AUTHORIZATION, String.format("%s %s", scheme, token));
        } else {
            System.out.println("Token is blank ????");
        }
        super.filter(requestContext);
        System.out.println("isAdmin : " + requestContext.getSecurityContext().isUserInRole("ADMIN"));
    }
}