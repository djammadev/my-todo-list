package com.loreal.security;

import com.avaje.ebean.Ebean;
import com.loreal.models.User;
import com.loreal.models.UserRole;
import com.shinitech.djammadev.forms.Credentials;
import com.shinitech.djammadev.models.STDefaultUser;
import com.shinitech.djammadev.models.STUser;
import com.shinitech.djammadev.security.Authenticator;
import com.shinitech.djammadev.services.security.BCrypt;

import java.util.List;

public class TodolistAuthenticator implements Authenticator {
    @Override
    public STUser authenticate(Credentials credentials) {
        User user = Ebean.find(User.class).where().eq("username", credentials.getUsername()).findUnique();
        STDefaultUser connected = null;
        if (user != null && BCrypt.checkpw(credentials.getPassword(), user.getPassword())) {
            connected = new STDefaultUser();
            connected.setFirstName(user.getFirstName());
            connected.setLastName(user.getLastName());
            connected.setUsername(user.getUsername());
            connected.setId(user.getId());
            List<UserRole> roleList = Ebean.find(UserRole.class).where().eq("username", user.getUsername()).findList();
            String[] roles = new String[roleList.size()];
            for (int i = 0; i < roles.length; i++) {
                roles[i] = roleList.get(i).role;
            }
            connected.setRoles(roles);
        }
        return connected;
    }
}