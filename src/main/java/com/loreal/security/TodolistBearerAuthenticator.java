package com.loreal.security;

import com.shinitech.djammadev.forms.Credentials;
import com.shinitech.djammadev.models.STUser;
import com.shinitech.djammadev.security.Authenticator;
import com.shinitech.djammadev.security.BearerAuthenticator;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

public class TodolistBearerAuthenticator implements Authenticator {
    private Authenticator underline;

    public TodolistBearerAuthenticator() {
        underline = new BearerAuthenticator(getSecret());
    }

    public String getSecret() {
        Config config = ConfigFactory.load();
        String secret = "";
        if (config.hasPath("api.key")) {
            secret = config.getString("api.key");
        }
        return secret;
    }

    @Override
    public STUser authenticate(Credentials credentials) {
        return underline.authenticate(credentials);
    }
}
