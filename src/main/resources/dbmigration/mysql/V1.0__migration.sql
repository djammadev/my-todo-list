-- apply changes
create table t_tasks (
  id                            bigserial not null,
  active                        boolean DEFAULT TRUE,
  created_by                    varchar(255) DEFAULT 'UNKNOW',
  modified_by                   varchar(255) DEFAULT 'UNKNOW',
  owner_id                      bigint,
  title                         varchar(255),
  description                   varchar(255),
  todolist_id                   bigint,
  create_date                   timestamp default CURRENT_TIMESTAMP not null,
  modify_date                   timestamp default CURRENT_TIMESTAMP not null,
  constraint pk_t_tasks primary key (id)
);

create table t_todo_lists (
  id                            bigserial not null,
  active                        boolean DEFAULT TRUE,
  created_by                    varchar(255) DEFAULT 'UNKNOW',
  modified_by                   varchar(255) DEFAULT 'UNKNOW',
  owner_id                      bigint,
  title                         varchar(255),
  user_id                       bigint,
  create_date                   timestamp default CURRENT_TIMESTAMP not null,
  modify_date                   timestamp default CURRENT_TIMESTAMP not null,
  constraint pk_t_todo_lists primary key (id)
);

create table t_users (
  id                            bigserial not null,
  active                        boolean DEFAULT TRUE,
  created_by                    varchar(255) DEFAULT 'UNKNOW',
  modified_by                   varchar(255) DEFAULT 'UNKNOW',
  owner_id                      bigint,
  username                      varchar(255),
  password                      varchar(255),
  first_name                    varchar(255) not null,
  last_name                     varchar(255) not null,
  create_date                   timestamp default CURRENT_TIMESTAMP not null,
  modify_date                   timestamp default CURRENT_TIMESTAMP not null,
  constraint uq_t_users_username unique (username),
  constraint pk_t_users primary key (id)
);

create table as_user_roles (
  id                            bigserial not null,
  active                        boolean DEFAULT TRUE,
  created_by                    varchar(255) DEFAULT 'UNKNOW',
  modified_by                   varchar(255) DEFAULT 'UNKNOW',
  owner_id                      bigint,
  username                      varchar(255) not null,
  role                          varchar(255) not null,
  user_id                       bigint,
  create_date                   timestamp default CURRENT_TIMESTAMP not null,
  modify_date                   timestamp default CURRENT_TIMESTAMP not null,
  constraint uq_as_user_roles_username_role unique (username,role),
  constraint pk_as_user_roles primary key (id)
);

alter table t_tasks add constraint fk_t_tasks_todolist_id foreign key (todolist_id) references t_todo_lists (id) on delete restrict on update restrict;
create index ix_t_tasks_todolist_id on t_tasks (todolist_id);

alter table t_todo_lists add constraint fk_t_todo_lists_user_id foreign key (user_id) references t_users (id) on delete restrict on update restrict;
create index ix_t_todo_lists_user_id on t_todo_lists (user_id);

alter table as_user_roles add constraint fk_as_user_roles_user_id foreign key (user_id) references t_users (id) on delete restrict on update restrict;
create index ix_as_user_roles_user_id on as_user_roles (user_id);

