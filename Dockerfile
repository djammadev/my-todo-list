FROM djammadev/djamminux:latest AS base

ENV APP_NAME my-todo-list

ENV PORT 80

WORKDIR /app

ADD .m2 /app/.m2
ADD src /app/src
ADD pom.xml /app/pom.xml

RUN mvn clean install -q -s .m2/settings.xml --batch-mode -Dmaven.repo.local=/app/.m2/repository

EXPOSE $PORT

CMD ["java", "$JAVA_OPTS", "-jar", "/app/target/dependency/webapp-runner.jar", "--port", "$PORT", "/app/target/${APP_NAME}.war"]
