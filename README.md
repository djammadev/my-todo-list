# archetype

`$ mvn archetype:generate -DarchetypeGroupId=com.shinitech.djammadev -DarchetypeArtifactId=djamma-architype -DarchetypeVersion=1.9.2-SNAPSHOT -DgroupId=org.example`

The command will create the folder tree :

```text
├── PORT
├── mysql.env
├── pom.xml
├── setenv.sh
└── src
    └── main
        ├── java
        │ └── org
        │     └── example
        │         ├── application
        │         │ └── MyApplication.java
        │         ├── models
        │         │ ├── MyModel.java
        │         │ └── finder
        │         │     └── MyModelFinder.java
        │         ├── modules
        │         │ └── MyModule.java
        │         └── resources
        │             └── MyResource.java
        ├── resources
        │ ├── application.conf
        │ ├── ebean.properties
        │ └── log4j.properties
        └── webapp
            ├── WEB-INF
            │ └── web.xml
            └── index.html
```

* Add files `.env`, `mysql.env`, `mdapi` into `.gitignore`
* Run `mvn clean install`
* Run migration to generate migration
* Run application `jersey restart`
* Go to [http://localhost:9999/docs](http://localhost:9999/docs)
